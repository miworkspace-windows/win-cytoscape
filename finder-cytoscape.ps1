function finderscript{

    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

    $html = ((Invoke-WebRequest -Uri "https://cytoscape.org/js/setup_page.js").RawContent).split("`r`n")
    $parsedContent = ($html | WHERE {$_ -like "*var CYTOSCAPE_LATEST_VERSION =*"}).split("'")[1]
    $replacedVersion = $parsedContent.replace(".","_")

    $content = "https://github.com/cytoscape/cytoscape/releases/download/$parsedContent/Cytoscape_$($replacedVersion)_windows_64bit.exe"
    apm-Winston -appName $appName -content $content -component "Finder"

    return $content
}
