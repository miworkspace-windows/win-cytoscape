#import scripts
$require = {. $_}
#autopackager scripts
Get-Item '.\autopackager\Class\*.ps1' | % $require
Get-Item '.\autopackager\functions\*.ps1' | % $require
Get-Item '.\autopackager\apmodules\*.ps1' | % $require
#finder-cytoscape
Get-Item '.\finder-cytoscape.ps1' | % $require
#postbuild for cytoscape
Get-Item '.\PostBuild.ps1' | % $require
([autoPackage]::new("Cytoscape")).build()