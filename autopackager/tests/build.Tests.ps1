Set-Location "..\"

#REGION Require external scripts
$require = {. $_}
Get-Item '.\Class\*.ps1' | % $require
Get-Item '.\functions\*.ps1' | % $require
Get-Item '.\apmodules\*.ps1' | % $require
Get-Item '..\finder-cytoscape.ps1' | % $require
Get-Item '..\postbuild.ps1' | % $require
#ENDREGION

Describe "build script"{

    
    Mock apm-winston {
        
        $output=@()
        $output+= "[string]`$component: $component`n"
        #if($appName){$output+= "[string]`$appName: $appName`n"}
        if($content){$output+= "[string]`$content: $content`n"}
        if($contentRepo){$output+= "[string]`$contentRepo: $contentRepo`n"}
        if($fileName){$output+= "[string]`$fileName: $fileName`n"}
        if($appinatorPath){$output+= "[string]`$appinatorPath: $appinatorPath`n"}
        if($logwatch){$output+= "[string]`$logwatch: $logwatch`n"}
        if($CleanUpInstallRepo){$output+= "[bool]`$CleanUpInstallRepo: $CleanUpInstallRepo`n"}
        if($PostBuildScriptFound){$output+= "[bool]`$PostBuildScriptFound: $PostBuildScriptFound`n"}
        if($pName){$output+= "[string]`$pName: $pName`n"}
        if($ErrorSettingAppinatorLocation){$output+= "[bool]`$ErrorSettingAppinatorLocation: $ErrorSettingAppinatorLocation`n"}
        if($productVersion){$output+= "[string]`$productVersion: $productVersion`n"}
        if($ErrorSettingJenkinsLocation){$output+= "[bool]`$ErrorSettingJenkinsLocation: $ErrorSettingJenkinsLocation`n"}
        if($PostBuildScriptNotFound){$output+= "[bool]`$PostBuildScriptNotFound: $PostBuildScriptNotFound`n"}
        if($AppVer){$output+= "[string]`$AppVer: $AppVer`n"}
        if($LogScrapeReturnedNull){$output+= "[bool]`$LogScrapeReturnedNull: $LogScrapeReturnedNull`n"}
        if($PostScrapeReturnedNull){$output+= "[bool]`$PostScrapeReturnedNull: $PostScrapeReturnedNull`n"}
        if($firing){$output+= "[bool]`$firing: $firing`n"}
        if($notFiring){$output+= "[bool]`$notFiring: $notFiring`n"}
        if($startWatch){$output+= "[bool]`$startWatch: $startWatch`n`n";return 0}
        if($return){$output+= "The final build script return code: [string]`$return: $return`n"}
        Write-Host $output
    }

    Mock apm-postbuild {<# do nothing #>}

    #Mock apm-downloadContent{Copy-Item "Z:\Appinator\CYTOSCAPE_3p5p1\Source\Cytoscape_3_5_1_windows_64bit.exe"}

    $global:testMe=""
    Mock apm-appinate{Write-Host "Appinating with the following `$arg: $arg `n";$global:testMe=$arg}

    Mock apm-exit{if($code -eq 0){Return "Exited with no errors"}}

    Mock clear-installRepo{Write-Host "Cleaning up install repo.`n"}

    Mock postbuild{Write-Host "Running Postbuild function."}
    
    It "downloads an exe and sends it's correct product version to appinator"{
    $autopackageInProgress = [autoPackage]::new("Cytoscape")
    $autopackageInProgress.build()
    $global:testMe | Should BeLike "*-AppVer `"$((get-item `"*.exe`").VersionInfo.ProductVersion)`"*"
    }
    Remove-Item "*.exe"
}