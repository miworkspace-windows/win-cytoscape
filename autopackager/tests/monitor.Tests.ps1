Set-Location "..\"

#REGION Require external scripts
$require = {. $_}
Get-Item '.\Class\*.ps1' | % $require
Get-Item '.\functions\*.ps1' | % $require
Get-Item '.\apmodules\*.ps1' | % $require
#ENDREGION

Describe "monitor script"{
    
    Mock apm-winston {
        
        if($component -eq "Finder"){Write-Host "Finder logging to Winston after scraping: $content"}
        if($AppVer){Write-Host "[string]`$AppVer: $AppVer"}
        if($productVer){Write-Host "[string]`$ProductVer: $ProductVer"}
        if($LogScrapeReturnedNull){Write-Host "[bool]`$LogScrapeReturnedNull: $LogScrapeReturnedNull"}
        if($PostScrapeReturnedNull){Write-Host "[bool]`$PostScrapeReturnedNull: $PostScrapeReturnedNull"}
        if($firing){Write-Host "[bool]`$firing: $firing"}
        if($notFiring){Write-Host "[bool]`$notFiring: $notFiring `n`l`n`l"}
    }
    
    $global:testMe=""
    Mock apm-exit{Write-Host "Exited with code: $code";$global:testMe=$code}

    It "exits with code 0 if the downloaded exe's ProductVer is higher than the AppVer which came from the current version packaged."{
    $autopackageInProgress = [autoPackage]::new("Cytoscape")
    $autopackageInProgress.monitor()
    $global:testMe | Should Be 0
    }
    Remove-Item "*.exe"
}