function apm-downloadContent{
    param($content, $filename)
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    
    Invoke-WebRequest -URI $content -OutFile $filename
}