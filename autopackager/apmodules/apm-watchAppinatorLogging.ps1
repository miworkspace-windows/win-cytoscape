function apm-watchAppinatorLogging{
    param
(
    [string]$APP_NAME,
    [string]$APP_VER
)
#Testing manual override of parameters.
#$APP_NAME = "Notepad++"

C:\Winston\corescripts\WinstonLog.ps1 -logstring "Watcher script started." -component "AppinatorLogging" -packageName $APP_NAME -type 1

#Gather the current date for log gathering later.
$startTime = Get-Date

#Create Slack message parameters
$pretext, $text, $color, $fallback

#Wait for logging to finish with a timeout of 20 minutes.
for($timeout = 0; $timeout -le 40; $timeout++)
{
    Start-Sleep -Seconds 30

    #C:\Winston\corescripts\WinstonLog.ps1 -logstring "Checking Appinator logging...  ($($timeout+1) of 40 ticks, ~30 second interval)" -component "TESTING!" -packageName $APP_NAME -type 1
    $log = C:\Winston\corescripts\Get-ObjectifiedLog.ps1 -LOG_PATH \\euc-sw.m.storage.umich.edu\euc-sw\logs\appinator-3.log -START_TIME $startTime -COMPONENT $APP_NAME

    #Check for errors or completion, if found, break.
    if(($log.entry -match "Error:") -or ($log.entry -match "Warning:Completed packaging process"))
    {
        #C:\Winston\corescripts\WinstonLog.ps1 -logstring "Found matching error or warning.  Attempting break." -component "TESTING!" -packageName $APP_NAME -type 1   
        break
    }
    #C:\Winston\corescripts\WinstonLog.ps1 -logstring "Done.  Sleeping for 30s." -component "TESTING!" -packageName $APP_NAME -type 1
}

#Check to see if we hit timeout.
if($timeout -ge 40)
{
    & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Appinator timeout of 20 minutes reached.  Sending exit 1 (failure) to Jenkins." -component "AppinatorLogging" -packageName $APP_NAME -type 3
    & C:\Winston\corescripts\Send-SlackPost.ps1 -pretext "Automated Testing has timed out." -text "Timeout of 20 minutes during automated packaging reached.  No error or warning checking done.  See logs." -color 'danger' -title $APP_NAME -version $APP_VER -fallbackText "Testing timeout limit exceeded for $APP_NAME $APP_VER"
    return 1
}

#Capture the entire Appinator log for this run.  Parse into errors and warnings.
$log = C:\Winston\corescripts\Get-ObjectifiedLog.ps1 -LOG_PATH \\euc-sw.m.storage.umich.edu\euc-sw\logs\appinator-3.log -START_TIME $startTime -COMPONENT $APP_NAME
$appinatorLogErrors = $log | ?{$_.entry -match "Error:"}
$appinatorLogWarnings = $log | ?{$_.entry -match "Warning:"}

#Check if the package completed.
$packageComplete = $false
if($appinatorLogWarnings | ?{$_.entry -match "Completed packaging process"})
{
    C:\Winston\corescripts\WinstonLog.ps1 -logstring "Detecting that Appinator logged completing the packaging process.  Setting complete flag." -component "AppinatorLogging" -packageName $APP_NAME -type 1
    $packageComplete = $true
}

#Remove the two warnings we don't care about -- CMD start and finish
$appinatorLogWarnings = $appinatorLogWarnings | ?{$_.entry -notmatch "Starting up cmdline function"}
$appinatorLogWarnings = $appinatorLogWarnings | ?{$_.entry -notmatch "Completed packaging process"}

#Log the number of errors and warnings found.
C:\Winston\corescripts\WinstonLog.ps1 -logstring "Appinator log error count: $($appinatorLogErrors.entry.count)" -component "AppinatorLogging" -packageName $APP_NAME -type 1
C:\Winston\corescripts\WinstonLog.ps1 -logstring "Appinator log warning count: $($appinatorLogWarnings.entry.count)" -component "AppinatorLogging" -packageName $APP_NAME -type 1

#Check for any errors.
if($appinatorLogErrors.entry.count -gt 0)
{
    #Additionally check for warnings.
    if($appinatorLogWarnings.entry.count -gt 0)
    {
        if($packageComplete)
        {
            $pretext = "Appinator automated packaging completed with errors and warnings."
            $color = "danger"
        }
        else
        {
            $pretext = "Appinator automated packaging failed due to errors, with warnings."
            $color = "danger"
        }
    }
    else
    {
        if($packageComplete)
        {
            $pretext = "Appinator automated packaging completed with errors."
            $color = "danger"
        }
        else
        {
            $pretext = "Appinator automated packaging failed due to errors."
            $color = "danger"
        }

    #Append all of the errors
    foreach($error in $appinatorLogErrors.entry)
    {
        $text = "$text`\n*ERROR:* $error"
    }

    #Append warnings, if any.
    if($appinatorLogWarnings.count -gt 0)
    {
        foreach($warning in $appinatorLogWarnings.entry)
        {
            $text = "$text`\n*WARNING*: $warning"
        }
    }

    #Post to HipChat, and exit with code 1 (failure) to Jenkins.
    & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Posting to Slack." -component "AppinatorLogging" -packageName $APP_NAME -type 1
    & C:\Winston\corescripts\Send-SlackPost.ps1 -pretext $pretext -text $text -title $APP_NAME -version $APP_VER -color $color -fallbackText "$pretext $APP_NAME $APP_VER"
    return 1
    }
}
else #Else, we don't have any errors, so we just have to check and handle warnings. It is assumed that no warning is terminating, so packaging completed.
{
    if($appinatorLogWarnings.entry.count -gt 0) #Check for any warnings.
    {
        #Set the base string and set the color
        $pretext= "Appinator automated packaging completed with warnings."
        $color = "warning"
        
        #Append all of the warnings
        foreach($warning in $appinatorLogWarnings.entry)
        {
            $text = "$text`\n*WARNING:* $warning"
        }
    }
    else #Else, we have no errors, and no warnings, which means we can post that packaging was done successfully.
    {
        $pretext = "Appinator automated packaging completed successfully!"
        $color = "good"
    }

    #Post to HipChat and exit with code 0 (success) to Jenkins.
    & C:\Winston\corescripts\WinstonLog.ps1 -logstring "Posting to Slack." -component "AppinatorLogging" -packageName $APP_NAME -type 1
    & C:\Winston\corescripts\Send-SlackPost.ps1 -pretext $pretext -text $text -title $APP_NAME -version $APP_VER -color $color -fallbackText "$pretext $APP_NAME $APP_VER"
    return 0
}
}