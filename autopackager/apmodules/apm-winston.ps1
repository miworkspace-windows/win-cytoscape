function apm-winston{
    param([string]$appName,[string]$content,[string]$component,[string]$contentRepo,[string]$fileName,[string]$appinatorPath,[string]$logwatch,[bool]$CleanUpInstallRepo,[bool]$PostBuildScriptFound,[string]$pName,[bool]$ErrorSettingAppinatorLocation,[string]$productVer,[bool]$ErrorSettingJenkinsLocation,[bool]$PostBuildScriptNotFound,[string]$return,[string]$AppVer,[bool]$LogScrapeReturnedNull,[bool]$PostScrapeReturnedNull,[bool]$firing,[bool]$notFiring,[bool]$startWatch,$arg)

    if($component -eq "Finder"){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "$appName $component run: $content" -packageName $appName -component $component -type 1}

    if($component -eq "Builder"){
        if($contentRepo){& C:\Winston\corescripts\Copy-RepoContent.ps1 -appName $appName -fileName $filename -contentRepo $contentRepo}
        if($appinatorPath){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Setting location to $appinatorPath and calling appinator script." -component "Builder" -packageName $appname -type 1}
        if($arg){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Sending Appinator the following arguments: $arg" -component "Builder" -packageName $appname -type 1}
        if($logwatch){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Starting Appinator log observer..." -component "Builder" -packageName $appname -type 1}
        if($CleanUpInstallRepo){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Due to return code 0, deleting !installerrepository content." -component "Builder" -packageName $appname -type 2}
        if($PostBuildScriptFound){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Post-build script found in workspace." -component "Builder" -packageName $appname -type 1}
        if($pName){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Setting location to \\euc-sw.m.storage.umich.edu\euc-sw\Appinator\$pName\Source" -component "Builder" -packageName $appname -type 1}
        if($ErrorSettingAppinatorLocation){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Error setting location to Appinator folder." -component "Builder" -packageName $appname -type 3}
        if($productVer){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Executing post-build script, providing version parameter: $productVer" -component "Builder" -packageName $appname -type 1}
        if($ErrorSettingJenkinsLocation){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Error setting location to Jenkins workspace." -component "Builder" -packageName $appname -type 3}
        if($PostBuildScriptNotFound){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Post-build script not found in workspace." -component "Builder" -packageName $appname -type 1}
        if($return){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Forwarding exit code $return to Jenkins." -component "Builder" -packageName $appname -type 1}
        if($StartWatch){apm-watchAppinatorLogging -APP_NAME $appName -APP_VER $appver -START_TIME ((Get-Date).AddHours(-18)) }
    }

    if($component -eq "Monitor"){
        if($AppVer){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "$appName current version: $($installProps.AppVer)" -packageName $appName -component "Monitor" -type 1}
        if($productVer){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "$appName detected version: $productVer" -packageName $appName -component "Monitor" -type 1}
        if($LogScrapeReturnedNull){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Scrape returned an empty or null value, or an error.  There's clearly a problem with the scrape." -packageName $appName -component "Monitor" -type 3}
        if($PostScrapeReturnedNull){& C:\Winston\corescripts\Send-SlackPost.ps1 -pretext "Scrape returned an empty or null value, or an error." -text "Please check the scraping function for this appliation." -title $appName -version "Error" -color danger -fallbackText "Scrape for $appName has failed."}
        if($firing){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Firing $appName build task..." -packageName "$appName" -component "Monitor" -type 2}
        if($notFiring){& C:\Winston\corescripts\WinstonLog.ps1 -logstring "Production version of $appName looks good." -packageName $appName -component "Monitor" -type 1}
    }
}