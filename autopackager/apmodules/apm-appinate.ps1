function apm-appinate{
    param($appinatorPath, $arg)
    Set-Location $appinatorPath
    Start-Process -FilePath powershell -ArgumentList "-file '.\Public\appinate.ps1  ' $arg"
}