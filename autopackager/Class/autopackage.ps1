class autopackage{
    $appName
    $contentRepo
    $mainExe
    $productVersion
    $propsPath
    $filename

    autopackage($appName){
        $this.appName = $appName
        $this.contentRepo = "\\euc-sw.m.storage.umich.edu\euc-sw\EUC\!Installps1\!installerrepository\$appName\latest"
        $this.propsPath = "\\euc-sw.m.storage.umich.edu\euc-sw\Appinator\!Appinator Install Media\$appName\installprops.JSON"
        
        #Call the Finder    
        $content = finderscript

        #Parse filename
        $this.filename = $(($content) -split "/")[-1]
        
        #Get content
        apm-downloadContent -content $content -filename $this.filename

        # Define main executable and its product version
        $this.mainExe = get-item $this.filename
        $this.productVersion = $this.mainExe.VersionInfo.ProductVersion

    }

    build()
    {
        #Copy content to Repo
        apm-winston -appName $this.appName -fileName $this.filename -contentRepo $this.contentRepo -component "Builder"

        #Execute the build and return
        apm-exit (run-build -appName $this.appName -filename $this.filename -contentRepo $this.contentRepo -productVersion ($this.productVersion) -installer $this.mainExe)

    }

    monitor()
     {
        #Read in installprops
        $installProps = Get-content $this.propsPath -Raw | ConvertFrom-Json

        #Log
        apm-winston -component "Monitor" -appName $this.appName -AppVer $installProps.AppVer
        apm-winston -component "Monitor" -appName $this.appName -productVer $this.productVersion

        #Check for an empty or null version.  This indicates an issue with the scrape.
        run-monitorPrecheck -appName $this.appName $this.productVersion

        #Compare versions to determine the status and exit
        run-monitor -appName $this.appName -appVer $installprops.appVer -productVersion $this.productVersion
    }
}