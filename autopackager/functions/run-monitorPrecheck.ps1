function run-monitorPrecheck{
    param($appName, $productVersion)
    
    if(($productVersion -eq $null) -or ($productVersion -like "") -or ($productVersion -match "error"))
    {
        apm-winston -component "Monitor" -appName $appName -LogScrapeReturnedNull $true
        apm-winston -component "Monitor" -appName $appName -PostScrapeReturnedNull $true    
        apm-exit 1
    }
}