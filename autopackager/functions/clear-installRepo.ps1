function clear-installRepo{
    param($appName)
    apm-winston -appName $appName -component "Builder" -CleanUpInstallRepo $true
    Remove-Item "\\euc-sw.m.storage.umich.edu\euc-sw\EUC\!Installps1\!installerrepository\$appName" -Recurse -Force
}