function run-build{
    param($filename, $appName, $productVersion, $contentrepo, $installer)
    

    #Create the path and arguments for the Appinator call
    $arg = "-AppName `"$appName`" -AppVer `"$productVersion`" -AppInstaller $filename"
    $appinatorPath = "D:\Jenkins\workspace\Windows\Builders\AppInator-3.0\alpha\" 
    
    #Log the parameters
    apm-winston -appName $appName -appinatorPath $appinatorPath -component "Builder"
    apm-winston -appName $appName -arg $arg -component "Builder"

    #Call Appinator.  Start-Process is helpful to properly spin up as another process.  Note we aren't waiting -- the wait happens on the Watch-AppinatorLogging call below.
    apm-appinate -appinatorPath $appinatorPath -arg $arg

    #Call the Appinator log watcher.  Note that this is piping into the $return variable, so it will wait to finish.
    apm-winston -appName $appName -component "Builder" -logWatch $true

    $return = apm-winston -appName $appName -component "Builder" -startWatch $true -appVer $productVersion

    #If successful, run post-build tasks.
    if($return -eq 0)
    {
        #Clean up the !installerrepository content
        clear-installRepo -appName $appName

        #Check for a post-build script
        if(Test-Path "postbuild-$appName.ps1")
        {
            apm-winston -appName $appName -component "Builder" -PostBuildScriptFound $true
            
            
            #Parse the name of the Appinator folder
            $pName = "$($appName)_$($productVersion -replace "\.", "p")" -replace " ", "_" -replace "\(|\)", ""

            #Set the location to the Appinator folder for the package we just made.
            apm-winston -appName $appName -component "Builder" -pName $pName
            
            try 
            {
                $jenkinsWorkspace = (Get-Location -errorAction Stop).path 
                Set-Location "\\euc-sw.m.storage.umich.edu\euc-sw\Appinator\$pName\Source" -errorAction Stop
            }
            catch
            {
                apm-winston -appName $appName -component "Builder" -ErrorSettingAppinatorLocation $true
            }

            #Call the post-build script.
            apm-winston -appName $appName -component "Builder" -productVersion $productVersion
            
            postbuild -newVer $productVersion
            

            #Return to Jenkins workspace
            try 
            {
                Set-Location $jenkinsWorkspace
            }
            catch
            {
                apm-winston -appName $appName -component "Builder" -ErrorSettingJenkinsLocation $true
            }
        }
        else
        {
            apm-winston -appName $appName -component "Builder" -PostBuildScriptNotFound $true
        }
    }
    
    postbuild -newVer $productVersion -appInstaller $installer
    
    #Return exit code to Jenkins.
    apm-winston -appName $appName -component "Builder" -return $return
    
    Return $return
}