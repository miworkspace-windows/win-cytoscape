function run-monitor{
    param($appName, $AppVer, $productVersion)
    #Compare versions to determine the status and exit
    if($([version]$AppVer) -lt $([version]$productVersion))
    {
        apm-winston -component "Monitor" -appName $appName -Firing $true
        apm-exit 0
    }
    else
    {
        apm-winston -component "Monitor" -appName $appName -notFiring $true
        
        apm-exit 1
    }        
}