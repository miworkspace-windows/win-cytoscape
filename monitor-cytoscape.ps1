#import scripts
$require = {. $_}
#Import autopackager scripts
Get-Item '.\autopackager\Class\*.ps1' | % $require
Get-Item '.\autopackager\functions\*.ps1' | % $require
Get-Item '.\autopackager\apmodules\*.ps1' | % $require
#Import finder-cytoscape
Get-Item '.\finder-cytoscape.ps1' | % $require
#Run Monitor for Cytoscape
([autoPackage]::new("Cytoscape")).monitor()