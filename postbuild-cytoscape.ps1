function postbuild{
    param ($newVer, $appInstaller)
    $props = get-content ".\installprops.JSON" -RAW | ConvertFrom-Json
    $props.detectscript = "if (test-path `"`"C:\Program Files\Cytoscape_v$newVer\Cytoscape.exe`"`"){		return `"`"true`"`"}else {}"
    $props.InstString = "$($props.AppInstaller) -quiet"
    $props.RemNewString = "C:\Program Files\Cytoscape_v$newVer\uninstall.exe -q"
    $props.SCDeletePath = "C:\ProgramData\Microsoft\\Windows\Start Menu\Programs\Cytoscape_$newVer\Cytoscape Uninstaller.lnk"
    $props | ConvertTo-Json| set-content ".\installprops.JSON" -Force
}